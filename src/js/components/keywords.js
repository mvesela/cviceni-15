import { __toggleClass, __getHTML, __dispatchEvent } from '../lib/utils';
import Component from './component';

// zoom the image
export default class Keywords extends Component {
  constructor($element, opts = {}) {
    super($element);
    this.id = this.target.id;
    this.function = this.target.getAttribute('data-keywords') || 'selekce-' || 'selekce+';
    this.$controls = this.target.querySelectorAll('[data-keywords-control]');

    this.opts = { ...Keywords.defaults, ...opts };

    this.$itemsContainer = this.target.querySelector(`.${this.opts.itemsContainerClassName}`);

    if (this.opts.inherit && this.opts.inherit.from.length > 0) {
      // console.log(this.opts.inherit.from);
      this.isInherited = true;
    }
    else {
      this.$items = this.target.querySelectorAll(`.${this.opts.itemClassName}`);
    }

  }

  init() {
    switch (this.function) {
      case 'selekce-':
      case 'selekce+':
        // we have data at the instantiation for importing
        if (this.opts.import && this.opts.import.length > 0) {
          this.$itemsContainer.innerHTML = Keywords._getHTMLFromItems(this.opts.import);
        }

        // console.log(this.id, this.isInherited);
        if (this.isInherited) {
          // if onInit, we copy the elements
          // by their ids into the itemsContainer of this object
          if (this.opts.inherit.onInit) {
            this._regenerateItemsContainerFromInherited();
          }
        }

        this._registerHandlers();
        break;
      case 'wordcloud':
        this.$items.forEach(($item) => {
          $item.setAttribute('data-keywords-item', 0);
        });
        this._changeSizeOfKeywordItem(this.opts.skala);
        break;

      default:
        break;
    }
  }

  _changeSizeOfKeywordItem(maxLevel) {
    maxLevel = maxLevel ? parseInt(maxLevel, 10) : 4;

    function setNewLevelValue($keyword, number, feedback) {
      const oldValue = parseInt($keyword.getAttribute('data-keywords-item'), 10);
      const newValue = oldValue + number;
      const value = Math.abs(newValue) > maxLevel ? oldValue : newValue;
      $keyword.setAttribute('data-keywords-item', value);

      feedback();
    }

    this.$controls.forEach(($control) => {
      const $keyword = $control.parentElement.parentElement;
      const controlFunction = $control.getAttribute('data-keywords-control');

      switch (controlFunction) {
        case 'minus':
          $control.addEventListener('click', (event) => {
            setNewLevelValue($keyword, -1, this.showFeedback);
            event.preventDefault();
          });
          break;

        case 'plus':
          $control.addEventListener('click', (event) => {
            setNewLevelValue($keyword, 1, this.showFeedback);
            event.preventDefault();
          });
          break;

        default:
          break;
      }
    });
  }

  _regenerateItemsContainerFromInherited() {
    let tempContent = '';

    this.opts.inherit.from.forEach((id) => {
      const temp = document.getElementById(id).querySelector(`.${this.opts.itemsContainerClassName}`).innerHTML;
      // rename ids of the original items to avoid duplicates in the DOM
      tempContent += Keywords.renameItemsIds(id, this.id, temp);
      // console.log(tempContent);
    });

    this.$itemsContainer.innerHTML = tempContent;

    this.$items = this.target.querySelectorAll(`.${this.opts.itemClassName}`);
  }

  _registerHandlers() {

    // this.target.querySelectorAll(`.${this.opts.itemClassName}`).forEach(($item) => {
    this.$items.forEach(($item) => {
      $item.addEventListener('click', this._clickEventHandler.bind(this), true);
      document.addEventListener('keyword.change', (e) => { this._dispatchEventHandler(e, $item); }, false);
    });

  }


  _clickEventHandler(e) {

    if (e.type === 'click') {

      __toggleClass(e.currentTarget, 'disabled');

      // dispatch event that keyword has changed
      __dispatchEvent(document, 'keyword.change', {}, {
        keywords: {
          id: this.id,
          node: e.currentTarget,
        },
      });

      this.showFeedback();
    }
  }

  _dispatchEventHandler(e, $item) {
    const { id, node } = e.detail.keywords;

    if (this.isInherited && this.opts.inherit.from.includes(id)) {
      const currentId = $item.id;
      const nodeId = node.id;
      const removeFeatureIdFromCurrentId = currentId.replace(this.id, '');
      const removeFeatureIdFromNodeId = nodeId.replace(id, '');

      if (removeFeatureIdFromCurrentId === removeFeatureIdFromNodeId) {
        __toggleClass($item, 'disabled');
      }
    }
  }
}


Keywords._getItemFromDOMElement = ($item) => ({
  class: $item.className,
  id: $item.getAttribute('id'),
  text: $item.innerText,
});

Keywords._getItemsHTML = ($itemsContainer) => $itemsContainer.innerHTML;

Keywords._getItemsFromHTML = ($itemsContainer, itemsClassName) => {
  const $items = $itemsContainer.querySelectorAll(`.${itemsClassName}`);
  const itemsArray = [];

  $items.forEach(($item) => {
    itemsArray.push(Keywords._getItemFromDOMElement($item));
  });


  return itemsArray;

};

Keywords._getDOMElementFromItem = (item) => {

  const newDiv = document.createElement('div');
  newDiv.setAttribute('class', item.class);
  newDiv.setAttribute('id', item.id);
  newDiv.innerHTML = item.text;

  return __getHTML(newDiv);

};

Keywords._getHTMLFromItems = (items) => {

  let contentHTML = '';

  items.forEach((item) => {

    contentHTML += Keywords._getDOMElementFromItem(item);

  });

  return contentHTML;

};


Keywords.renameItemsIds = (oldId, newId, itemsHTML) => {

  const expString = new RegExp(oldId, 'g');

  return itemsHTML.replace(expString, newId);

};

Keywords.defaults = {
  itemsContainerClassName: 'keywords-items-container',
  itemClassName: 'keywords-item',
};
